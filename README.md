# fuck-kafka

do what the fuck with kafka

### What should I take notes
- topic will be auto-created with `@KafkaListener`
- topic will be auto-created for message sent with `KafkaTemplate.send(String, V)` .
- To have the correct broker address set during each test case,
we need to use the `@DirtiesContext` on all test classes.
The reason for this is that each test case contains its own embedded Kafka broker that will each be created on a new random port.
By rebuilding the application context, the beans will always be set with the current broker address.
