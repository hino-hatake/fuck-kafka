FROM openjdk:11-jre-slim
MAINTAINER hino<hino-hatake@gitlab.com>

VOLUME /tmp
COPY target/*.jar app.jar
ENV ACTIVE_PROFILE=default

ENTRYPOINT ["java","-jar","/app.jar","--spring.profiles.active=${ACTIVE_PROFILE}"]
