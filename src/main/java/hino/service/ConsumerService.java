package hino.service;

public interface ConsumerService {

  void listen(String message);

  void listen(String message, int partition);
}
