package hino.service;

public interface ProducerService {

  void sendMessage(String topic, String message);

  void sendMessage(String topic, String key, String message);
}
