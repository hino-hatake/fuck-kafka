package hino.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ConsumerServiceImpl implements ConsumerService {

  @Override
  @KafkaListener(topics = "fuck", groupId = "foo")
  public void listen(String message) {
    log.info("Received message: [{}]", message);
  }

  @Override
  @KafkaListener(topics = "fuck", groupId = "foo")
  public void listen(@Payload String message, @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {
    log.info("Received message: [{}] from partition: [{}]", message, partition);
  }
}
