package hino.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
class ProducerServiceImpl implements ProducerService {

  @Autowired
  private KafkaTemplate<String, String> kafkaTemplate;

  @Override
  public void sendMessage(String topic, String message) {
    sendMessage(topic, null, message);
  }

  @Override
  public void sendMessage(String topic, String key, String message) {
    var future = kafkaTemplate.send(topic, key, message);

    // handle the results asynchronously with callback, simplify with lambda
    future.addCallback(
        // onSuccess
        result -> log.info("Sent message=[{}] to topic=[{}] with offset=[{}]",
            message, result.getProducerRecord().topic(), result.getRecordMetadata().offset()),
        // onFailure
        ex -> log.info("Unable to send message=[{}] due to : {}", message, ex.getMessage()));
  }
}
