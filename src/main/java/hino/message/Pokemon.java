package hino.message;

import hino.common.PokemonType;
import java.util.List;
import lombok.Data;

@Data
public class Pokemon {

  private String name;

  private List<PokemonType> types;

  /**
   * base stats in total
   */
  private int stat;

  private List<String> moves;
}
