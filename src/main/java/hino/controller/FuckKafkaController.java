package hino.controller;

import hino.service.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("kafka")
public class FuckKafkaController {

  private final ProducerService producerService;

  @Autowired
  public FuckKafkaController(ProducerService producerService) {
    this.producerService = producerService;
  }

  @PostMapping(value = "/publish")
  public void sendMessageToKafkaTopic(@RequestParam("topic") String topic, @RequestParam("message") String message) {
    this.producerService.sendMessage(topic, message);
  }
}
