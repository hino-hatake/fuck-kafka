package hino.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ProducerServiceTest {

  private final ProducerService producerService;

  @Autowired
  public ProducerServiceTest(ProducerService producerService) {
    this.producerService = producerService;
  }

  @Test
  void testSendMessage_topicAvailable() {
    producerService.sendMessage("producer", "hihi");
  }

  @Test
  void testSendMessage_topicNotAvailable() {
    producerService.sendMessage("hack", "hihi");
  }
}
